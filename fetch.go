package webclient

import (
	"crypto/tls"
	"log"
	"net/http"
	"net/http/cookiejar"
	"net/url"
	"strings"
	"time"

	"gitlab.com/alitop/parameters"
)

// Fetch is function
func Fetch(useragent, URL, Referer, locale string, resp *http.Response, cookieJar *cookiejar.Jar, method, contentType string, reader *strings.Reader) (*http.Response, *cookiejar.Jar, error) {
	var (
		req *http.Request
		err error
	)
	if cookieJar == nil {
		cookieJar, _ = cookiejar.New(nil)
	}
	cookies := newDesktopCookie(locale)
	u, _ := url.Parse(URL)
	cookieJar.SetCookies(u, cookies)
	for i := 0; i < parameters.MaxNumberOfAttempts; i++ {
		var innerErr error
		tr := &http.Transport{
			TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
		}
		if reader != nil {
			req, innerErr = http.NewRequest(method, URL, reader)
		} else {
			req, innerErr = http.NewRequest(method, URL, nil)
		}
		if innerErr != nil {
			log.Println(innerErr)
			err = innerErr
			continue
		}
		req.Header.Set("User-Agent", useragent)
		req.Header.Set("Content-Type", contentType)
		if Referer == "" {
			Referer = "http://coupon.aliexpress.com"
		}
		req.Header.Set("Referer", Referer)
		// NOTE this !!
		req.Close = true
		timeout := parameters.ScrapTimeout
		client := &http.Client{
			Transport: tr,
			Timeout:   time.Duration(timeout) * time.Second,
			Jar:       cookieJar,
			CheckRedirect: func(req *http.Request, via []*http.Request) error {
				return http.ErrUseLastResponse
			},
		}
		resp, innerErr = client.Do(req)
		if innerErr != nil || resp == nil {
			cookieJar, _ = cookiejar.New(nil)
			cookies := newDesktopCookie(locale)
			u, _ := url.Parse(URL)
			cookieJar.SetCookies(u, cookies)
			useragent, innerErr = GetDesktopUseragent()
			if innerErr != nil {
				log.Println(innerErr)
				err = innerErr
			}
			if resp != nil {
				resp.Body.Close()
			}
			time.Sleep(100 * time.Millisecond)
			continue
		}
		if resp.StatusCode == http.StatusFound || resp.StatusCode == http.StatusMovedPermanently {
			redirectURL, _ := resp.Location()
			Referer = URL
			URL = redirectURL.String()
			continue
		}
		err = innerErr
		break
	}
	return resp, cookieJar, err
}

func newDesktopCookie(locale string) []*http.Cookie {
	var cookies []*http.Cookie
	aep_usuc_f := "site=rus&c_tp=USD&region=RU&b_locale=ru_RU"
	intl_locale := "ru_RU"
	if locale == "en" {
		aep_usuc_f = "site=glo&c_tp=USD&region=RU&b_locale=en_US"
		intl_locale = "en_US"
	}
	cookie := &http.Cookie{
		Name:   "aep_usuc_f",
		Value:  aep_usuc_f,
		Path:   "/",
		Domain: ".aliexpress.com",
	}
	cookies = append(cookies, cookie)
	cookie = &http.Cookie{
		Name:   "intl_locale",
		Value:  intl_locale,
		Path:   "/",
		Domain: ".aliexpress.com",
	}
	cookies = append(cookies, cookie)
	cookie = &http.Cookie{
		Name:   "xman_us_f",
		Value:  "x_locale=" + intl_locale + "&x_l=0",
		Path:   "/",
		Domain: ".aliexpress.com",
	}
	cookies = append(cookies, cookie)
	return cookies
}
