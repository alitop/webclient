package webclient

import (
	"fmt"
	"io"
	"log"
	"math/rand"
	"net/http"
	"os"
	"strings"
	"sync"
	"time"

	"gitlab.com/alitop/helpers"
	"gitlab.com/alitop/parameters"
)

var (
	ArrayGoodProxies       []string
	ArrayProxies           []string
	arrayUserAgentsDesktop []string
	arrayUserAgentsMobile  []string
	encountered            map[string]bool
	isInit                 bool
	mutexGoodProxy         sync.RWMutex
	mutexUserAgentDesktop  sync.RWMutex
	mutexUserAgentMobile   sync.RWMutex
	mutexProxy             sync.RWMutex
	mutexGetProxy          sync.RWMutex
	startTime              time.Time
	TotalProxy             int
)

// GetDesktopUseragent get new desktop user-agent
func GetDesktopUseragent() (string, error) {
	mutexUserAgentDesktop.Lock()
	var err error
	var ua string
	if len(arrayUserAgentsDesktop) < 2 {
		arrayUserAgentsDesktop, err = helpers.ReadLines(os.Getenv("FILE_UA_DESKTOP"))
		if err != nil {
			mutexUserAgentDesktop.Unlock()
			return os.Getenv("UA_DESKTOP"), err
		}
	}
	s := rand.NewSource(time.Now().Unix())
	r := rand.New(s)
	index := r.Intn(len(arrayUserAgentsDesktop))
	ua = arrayUserAgentsDesktop[index]
	arrayUserAgentsDesktop = append(arrayUserAgentsDesktop[:index], arrayUserAgentsDesktop[index+1:]...)
	mutexUserAgentDesktop.Unlock()
	return ua, nil
}

// GetMobileUseragent get new desktop user-agent
func GetMobileUseragent() (string, error) {
	mutexUserAgentMobile.Lock()
	var err error
	var ua string
	if len(arrayUserAgentsMobile) < 2 {
		arrayUserAgentsMobile, err = helpers.ReadLines(os.Getenv("FILE_UA_MOBILE"))
		if err != nil {
			mutexUserAgentMobile.Unlock()
			return os.Getenv("UA_MOBILE"), err
		}
	}
	s := rand.NewSource(time.Now().Unix())
	r := rand.New(s)
	index := r.Intn(len(arrayUserAgentsMobile))
	ua = arrayUserAgentsMobile[index]
	arrayUserAgentsMobile = append(arrayUserAgentsMobile[:index], arrayUserAgentsMobile[index+1:]...)
	mutexUserAgentMobile.Unlock()
	return ua, nil
}

func GetProxy() (string, error) {
	var (
		err   error
		proxy string
	)
	mutexGetProxy.Lock()
	duration := time.Since(startTime)
	if len(ArrayProxies) == 0 {
		if isInit == false || len(ArrayGoodProxies) == 0 || duration.Minutes() > parameters.DurationProxyMinutes {
			isInit = true
			if duration.Minutes() > parameters.DurationProxyMinutes {
				log.Println(time.Now().Format(os.Getenv("LOG_TIME_FORMAT")), "Get New File")
				var out *os.File
				var resp *http.Response
				out, err = os.Create(os.Getenv("FILE_AWMPROXY_FILE"))
				for i := 0; i < parameters.MaxNumberOfAttempts; i++ {
					proxyURL := os.Getenv("URL_PROXY_LIST")
					if proxyURL == "" {
						proxyURL = "https://awmproxy.com/proxy/fa9708119cd0eb6f25f1ea8b284dfb5f"
					}
					resp, err = http.Get(proxyURL)
					if err != nil {
						log.Println(time.Now().Format(os.Getenv("LOG_TIME_FORMAT")), i, err)
						time.Sleep(time.Millisecond * 1000)
						continue
					}
					_, err = io.Copy(out, resp.Body)
					if err != nil {
						log.Println(time.Now().Format(os.Getenv("LOG_TIME_FORMAT")), i, err)
						time.Sleep(time.Millisecond * 300)
						continue
					}
					out.Close()
					resp.Body.Close()
					break
				}
				startTime = time.Now()
			}
			// Read from file
			encountered = make(map[string]bool)
			ArrayProxies, err = helpers.ReadLines(os.Getenv("FILE_AWMPROXY_FILE"))
			if err != nil {
				log.Println(time.Now().Format(os.Getenv("LOG_TIME_FORMAT")), err.Error())
			}
			for k := range ArrayProxies {
				src := rand.NewSource(time.Now().UnixNano())
				r := rand.New(src)
				l := r.Intn(k + 1)
				ArrayProxies[k], ArrayProxies[l] = ArrayProxies[l], ArrayProxies[k]
			}
			TotalProxy = len(ArrayProxies)
			ArrayGoodProxies = ArrayGoodProxies[:0]
		} else {
			if parameters.IgnoreGoodProxy == true {
				ArrayProxies, err = helpers.ReadLines(os.Getenv("FILE_AWMPROXY_FILE"))
				if err != nil {
					log.Println(time.Now().Format(os.Getenv("LOG_TIME_FORMAT")), err.Error())
				}
			} else {
				ArrayProxies = ArrayGoodProxies
				ArrayGoodProxies = ArrayGoodProxies[:0]
			}
			isInit = false
		}
	}
	if len(ArrayProxies) == 0 {
		fmt.Printf("encountered: %+v\n", encountered)
		fmt.Printf("parameters.IgnoreGoodProxy: %+v\n", parameters.IgnoreGoodProxy)
		fmt.Printf("ArrayGoodProxies: %+v\n", len(ArrayGoodProxies))
		fmt.Printf("isInit: %+v\n", isInit)
		fmt.Printf("startTime: %v\n", startTime)
		fmt.Printf("duration.Minutes(): %+v\n", duration.Minutes())
		fmt.Printf("parameters.DurationProxyMinutes: %+v\n", parameters.DurationProxyMinutes)
		stat, err := os.Stat(os.Getenv("FILE_AWMPROXY_FILE"))
		if os.IsNotExist(err) {
			fmt.Println(time.Now().Format(os.Getenv("LOG_TIME_FORMAT")), "File not exist")
		} else {
			modTime := stat.ModTime()
			duration := time.Since(modTime).Minutes()
			if duration > parameters.DurationProxyMinutes {
				log.Printf("%s file must be update:\n%+v\n", time.Now().Format(os.Getenv("LOG_TIME_FORMAT")), stat)
			} else {
				log.Printf("%s use exist file:\n%+v\n", time.Now().Format(os.Getenv("LOG_TIME_FORMAT")), stat)
			}
		}
	}
	if len(ArrayProxies) == 0 {
		ArrayProxies, err = helpers.ReadLines(os.Getenv("FILE_AWMPROXY_FILE"))
		if err != nil {
			log.Println(time.Now().Format(os.Getenv("LOG_TIME_FORMAT")), err.Error())
		}
	}
	proxy = ArrayProxies[0]
	ArrayProxies = append(ArrayProxies[:0], ArrayProxies[1:]...)
	endsWith := strings.HasSuffix(proxy, ".")
	if endsWith {
		proxy = proxy[:len(proxy)-1]
	}
	if strings.Contains(proxy, "://") == false {
		proxy = "http://" + proxy
	}
	mutexGetProxy.Unlock()
	if parameters.UseTor == true {
		return "http://127.0.0.1:5566", err
	}
	return proxy, err
}

// AppendGoodProxy is function
func AppendGoodProxy(ProxyAddr string) {
	mutexGoodProxy.Lock()
	// log.Println(ProxyAddr)
	if encountered[ProxyAddr] == true {
		// Do not add duplicate.
	} else {
		// Record this element as an encountered element.
		encountered[ProxyAddr] = true
		ArrayGoodProxies = append(ArrayGoodProxies, ProxyAddr)
		// log.Println(len(ArrayGoodProxies))
	}
	mutexGoodProxy.Unlock()
}
